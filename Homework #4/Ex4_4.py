import pandas as pd
import random
import random as np

ids = ['id1', 'id2', 'id3', 'id4', 'id5', 'id6', 'id7', 'id8',
       'id9', 'id10', 'id11', 'id12', 'id13', 'id14', 'id15',
       'id16', 'id17', 'id18', 'id19', 'id20']

def index(X):
       index = []
       while len(index) < 10:
              t = random.choice(X)
              if t not in index:
                     index.append(t)
       return index

def _index():
       index = []
       while len(index) < 10:
              t = random.randint(-100,101)
              if t not in index:
                     index.append(t)
       return index

S1_series = pd.Series(index(ids), name = 'ids1')
S2_series = pd.Series(index(ids), name = 'ids2')
S3_series = pd.Series(_index(), name = 'data1')
S4_series = pd.Series(_index(), name = 'data2')

dataframe1 = pd.DataFrame({'key': S1_series, 'data1': S3_series})
dataframe2 = pd.DataFrame({'key': S2_series, 'data2': S4_series})

dataframe = pd.merge(dataframe1, dataframe2, on= ['key'])

print('S1 : \n', S1_series)
print('-------------------------- \n S2 : \n',S2_series)
print('-------------------------- \n S3 : \n',S3_series)
print('-------------------------- \n S4 : \n',S4_series)

print('--------------------------- \n Build 2 DataFrame from those Series')
print('dataframe1: \n', dataframe1)
print('--------------------------- \n dataframe2: \n', dataframe2)

print('---------------------------- \n A join on those two dataframe')
print(dataframe)

