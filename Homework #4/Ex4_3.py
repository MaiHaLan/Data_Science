import pandas as pd
import numpy as np

# Generate a DataFrame
def _dataframe(_start, _periods, _freq):
    frame_data = pd.DataFrame({'Month' : pd.date_range(start = _start, periods = _periods, freq = _freq),
                         'Sale' : np.random.randint(10,30, (_periods,))})
    return frame_data
frame_data_year = _dataframe('31/1/2018', 50, 'M')
print(frame_data_year)

# Calculate the sum of sale by year
years = []
for i in range(len(frame_data_year)):
    years.append(frame_data_year.iloc[i][0].year)
frame_data_year['Year'] = years

print('-------------------------------- \n Calculate the sum of sale by year')
print(frame_data_year.groupby(['Year']).sum())

# Calculate the sum of sale by month
frame_data_month = _dataframe('31/1/2018', 200, 'D')

months = []
for i in range(len(frame_data_month)):
    months.append(frame_data_month.iloc[i][0].month)
frame_data_month['Month'] = months

print('-------------------------------- \n Calculate the sum of sale by month')
print(frame_data_month.groupby(['Month']).sum())