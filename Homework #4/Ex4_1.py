import pandas as pd

_string = '''Ah meta descriptions… the last bastion of traditional marketing! 
The only cross-over point between marketing and search engine optimisation! 
The knife edge between beautiful branding and an online suicide note!'''

series_words = pd.Series(_string.split(), name = 'word')
print(series_words)

vowels = set('aoieu')

_list = []
for i in range(len(series_words)):
    word = series_words[i]
    k = 0
    for j in vowels:
        k += word.lower().count(j)
    if k >= 3:
        _list.append(word)
print('All words that contains at least 3 vowels: ', _list)